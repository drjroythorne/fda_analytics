#!/usr/bin/env python

from setuptools import setup, find_packages

setup(
    name="fda_analytics",
    author="Daniel Roythorne",
    author_email="dan@droythorne.com",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Natural Language :: English",
        "Programming Language :: Python :: 3.6",
    ],
    description="FDA API analytics",
    entry_points={"console_scripts": []},
    install_requires=[],
    license="Proprietary",
    include_package_data=True,
    keywords="fda_analytics",
    packages=find_packages("src"),
    package_dir={"": "src"},
    setup_requires=["pytest-runner"],
    test_suite="tests",
    tests_require=["pytest"],
    url="https://gitlab.com/droythorne/fda_analytics",
    version="0.0.3",  # Modify with bumpversion
)
