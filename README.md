# fda\_analytics

FDA API analytics

## Features

TODO: 

## Data source

Limits on the OpenFDA endpoint make it tricky to work with (e.g. 100 record paging, cannot specify output fields), but there are a manageable number of downloadable files (~1GB) under the relevant endpoint (/drug/label.json) listed at https://api.fda.gov/download.json, so opt to download the data and work locally for exploring the dataset.

## Data quality

As mentioned in the Github gist, the `spl_product_data_elements` field is free-form text, with no systematic delimiters. Barring access to a definitive list of pharaceutical ingredients to pattern match against, seeking out alternative sources for the ingredient count was an option. From the fields reference document, candidates considered were:

- '.openfda.unii': rejected, since this seems to only be active ingredients;
- '.active\_ingredient': this field was not populated for AstraZeneca records.
- '.inactive\_ingredient': this field was not populated for AstraZeneca records.
