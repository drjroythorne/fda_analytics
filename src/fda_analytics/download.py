import json
from pathlib import Path

import jq
import requests


_FDA_DOWNLOADS_URL = "https://api.fda.gov/download.json"
_DRUG_LABEL_FILTER = jq.jq(".results.drug.label.partitions| .[] | .file")


def get_openfda_drug_label_urls(fda_downloads_url=_FDA_DOWNLOADS_URL):
    response = requests.get(_FDA_DOWNLOADS_URL)

    if response.status_code == 200:
        response_json = json.loads(response.content.decode("utf-8"))
        return _DRUG_LABEL_FILTER.transform(response_json, multiple_output=True)


def download_drug_labels(url, output_directory):
    output_file_path = _create_output_file_path(
        url=url, output_directory=Path(output_directory)
    )
    _download_file(url=url, output_file_path=output_file_path)
    return output_file_path


def _create_output_file_path(url, output_directory):
    return output_directory / Path(url.split("/")[-1])


def _download_file(url, output_file_path):
    response = requests.get(url, stream=True)

    if response.status_code == 200:
        with open(output_file_path, "wb") as f:
            for chunk in response.iter_content(1024):
                f.write(chunk)
