import numpy as np
import pandas as pd


def calculate_average_number_of_ingredients_per_year(ingredients):
    return _calculate_average_number_of_ingredients_per_group(
        ingredients, grouping_variables=["year"]
    )


def calculate_average_number_of_ingredients_per_year_per_route(ingredients):
    return _calculate_average_number_of_ingredients_per_group(
        ingredients.explode(".openfda.route"),
        grouping_variables=["year", ".openfda.route"],
    )


def _concatenate_brand_names(brand_names):
    return ",".join(brand_names)


def _calculate_average_number_of_ingredients_per_group(ingredients, grouping_variables):
    # Check all required columns are present
    for variable in grouping_variables:
        assert variable in ingredients.columns
    assert "number_of_ingredients" in ingredients.columns
    assert "brand_name" in ingredients.columns

    result = ingredients.groupby(grouping_variables).agg(
        avg_number_of_ingredients=pd.NamedAgg("number_of_ingredients", np.mean),
        support=pd.NamedAgg("brand_name", "count"),
        drug_names=pd.NamedAgg("brand_name", _concatenate_brand_names),
    )
    return result
