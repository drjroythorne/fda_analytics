import re

import pandas as pd


def append_features(ingredients):
    """ Add features for subsequent analysis.
    
    :param pd.DataFrame ingredients:
    :return void:
    """
    # Extract manufacturer's name from list.
    ingredients["manufacturer_name"] = ingredients[".openfda.manufacturer_name"].map(
        _checked_only_element_if_not_none
    )
    # Extract brand name from list.
    ingredients["brand_name"] = ingredients[".openfda.brand_name"].map(
        _checked_only_element_if_not_none
    )
    # Count number of ingredients according to OpenFDA unii field.
    ingredients["number_of_uniis"] = ingredients[".openfda.unii"].map(
        len, na_action="ignore"
    )
    # Parse effective_time to Python datetime.
    ingredients["effective_time_datetime"] = pd.to_datetime(
        ingredients[".effective_time"], format="%Y%m%d", errors="coerce"
    )
    # Extract the effective_time year.
    ingredients["year"] = ingredients["effective_time_datetime"].map(
        lambda d: d.year, na_action="ignore"
    )
    # Determine the number of ingredients from the spl_product_data_elements field.
    ingredients["number_of_ingredients"] = ingredients[
        ".spl_product_data_elements"
    ].map(_number_of_separate_ingredients, na_action="ignore")


def _checked_only_element_if_not_none(l):
    """ Return the element from single element list.
    
    :param list|None l:
    :return mixed: 
    """
    if l is None:
        return None

    assert len(l) == 1
    return l[0]


def _number_of_separate_ingredients(spl_product_data_elements):
    """ Determine the number of ingredients from the spl_product_data_elements field.
    
    :param str spl_product_data_elements:
    :return int:
    """
    return len(re.split(",|;", " ".join(spl_product_data_elements)))
