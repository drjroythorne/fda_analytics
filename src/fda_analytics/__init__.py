import logging

import pandas as pd


__author__ = """Daniel Roythorne"""
__email__ = "dan@droythorne.com"
__version__ = "0.0.3"  # modify with bumpversion


pd.options.display.max_colwidth = 1000


def configure_logging():
    logger = logging.getLogger("fda_analytics")
    logger.setLevel(logging.DEBUG)
    sh = logging.StreamHandler()
    formatter = logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    )
    sh.setFormatter(formatter)
    logger.addHandler(sh)
    return logger
