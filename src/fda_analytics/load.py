import json
import zipfile

import jq
import pandas as pd


_DRUG_LABEL_JSON_SELECTORS = [
    ".openfda.brand_name",
    ".effective_time",
    ".spl_product_data_elements",
    ".openfda.manufacturer_name",
    ".openfda.route",
    ".openfda.unii",
    ".active_ingredient",
    ".inactive_ingredient",
]


def load_ingredients_data_frame(file_paths):
    ingredients_data_frame_generator = (
        _extract_ingredients_data_frame(
            labels_json=_load_labels(path), selectors=_DRUG_LABEL_JSON_SELECTORS
        )
        for path in file_paths
    )
    return pd.concat(ingredients_data_frame_generator)


def _load_labels(archive_file_path):
    with zipfile.ZipFile(archive_file_path, "r") as z:
        for json_filename in z.namelist():
            with z.open(json_filename, "r") as f:
                return json.load(f)


def _extract_ingredients_data_frame(labels_json, selectors):
    fields_query = "[{}]".format(",".join(selectors))
    field_extractor = jq.jq(fields_query)

    ingredients_records = [
        field_extractor.transform(record) for record in labels_json["results"]
    ]

    return pd.DataFrame(ingredients_records, columns=selectors)
