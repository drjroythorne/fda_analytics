def filter_by_manufacturer_name(ingredients, manufacturer_name):
    """
    :param pandas.DataFrame ingredients:
    :param str manufacturer_name:
    """
    return ingredients[
        ingredients["manufacturer_name"].str.contains(manufacturer_name, na=False)
    ]
